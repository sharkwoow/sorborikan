<?php
session_start();
// Check if the Add to Cart button is clicked
if (isset($_POST['add_to_cart'])) {
    $product_id = $_POST['product_id'];

    // Check if the cart session variable is set
    if (!isset($_SESSION['cart'])) {
        $_SESSION['cart'] = array();
    }

    // Check if the product is already in the cart
    if (isset($_SESSION['cart'][$product_id])) {
        // If it is, increase the quantity
        $_SESSION['cart'][$product_id]++;
        echo $_SESSION['cart'][$product_id]++;
    } else {
        // If not, add the product to the cart with quantity 1
        $_SESSION['cart'][$product_id] = 1;
        echo $_SESSION['cart'][$product_id] = 1;
    }
} else {
    echo "nothing to do";
}
echo ($_POST['add_to_cart']);

// Display the cart content
echo "<h2>Shopping Cart</h2>";
echo "<ul>";
$total_price = 0;

// Loop through each item in the cart
foreach ($_SESSION['cart'] as $product_id => $quantity) {
    // Retrieve product information (this is just a simple example)
    $product_name = "Product " . $product_id;
    $product_price = $quantity * 10; // Assuming each product costs $10

    // Display the item in the cart
    echo "<li>$product_name - Quantity: $quantity - Price: $product_price</li>";

    // Update the total price
    $total_price += $product_price;
}

echo "</ul>";
echo "<p>Total Price: $total_price</p>";
