<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Shopping Cart</title>
    <link rel="stylesheet" href="cart.css" </head>

<body>

    <h2>Product List</h2>

    <!-- Product 1 -->
    <form method="post" action="cart.php">
        <input type="text" name="product_id" value="1">
        <p>Product 1 - $10</p>
        <input type="submit" name="add_to_cart" value="Add to Cart">
    </form>

    <!-- Product 2 -->
    <form method="post" action="cart.php">
        <input type="text" name="product_id" value="2">
        <p>Product 2 - $20</p>
        <input type="submit" name="add_to_cart" value="Add to Cart">
    </form>
    <div>no more anything</div>

</body>

</html>